#include "header.h"

Parameters *init_parameters(void)
{
  Parameters *p = malloc(sizeof(Parameters));

  p->n_particles = 1000000;
  p->n_batches = 10;
  p->n_generations = 1;
  p->n_active = 10;
  p->bc = REFLECT;
  p->n_nuclides = 1;
  p->tally = TRUE;
  p->n_bins = 16;
  p->seed = 1;
  p->nu = 2.5;
  p->xs_f = 0.012;
  p->xs_a = 0.03;
  p->xs_s = 0.27;
  p->gx = 400;
  p->gy = 400;
  p->gz = 400;
  p->write_tally = FALSE;
  p->write_keff = FALSE;
  p->tally_file = NULL;
  p->keff_file = NULL;
#ifdef USEMPI
  p->mpi_nx = 1; // 1 domain in x dimension by default 
  p->mpi_ny = 1; // 1 domain in y dimension by default 
  p->mpi_nz = 1; // 1 domain in z dimension by default 
#endif
  return p;
}

Geometry *init_geometry(Parameters *parameters)
{
  Geometry *g = malloc(sizeof(Geometry));

  g->x = parameters->gx;
  g->y = parameters->gy;
  g->z = parameters->gz;
  g->bc = parameters->bc;
#ifdef USEMPI
  mpi.nx = parameters->mpi_nx;
  mpi.ny = parameters->mpi_ny;
  mpi.nz = parameters->mpi_nz;
  mpi.k = mpi.rank/(mpi.nx*mpi.ny);
  int resid = mpi.rank%(mpi.nx*mpi.ny);
  mpi.j = resid/mpi.nx;
  mpi.i = resid%mpi.nx;
  g->dx = g->x/mpi.nx;
  g->dy = g->y/mpi.ny;
  g->dz = g->z/mpi.nz;
  g->x_min = mpi.i*g->dx;
  g->x_max = (mpi.i + 1)*g->dx;
  g->y_min = mpi.j*g->dy;
  g->y_max = (mpi.j + 1)*g->dy;
  g->z_min = mpi.k*g->dz;
  g->z_max = (mpi.k + 1)*g->dz;
#endif
  return g;
}

Tally *init_tally(Parameters *parameters)
{
  Tally *t = malloc(sizeof(Tally));

  t->tallies_on = FALSE;
  t->n = parameters->n_bins;
#ifdef USEMPI
  t->nx = t->n/mpi.nx;
  if (t->n%mpi.nx != 0)
  {
      print_error("Wrong number of bins in x dimension");
  }
  t->ny = t->n/mpi.ny;
  if (t->n%mpi.ny != 0)
  {
      print_error("Wrong number of bins in y dimension");
  }
  t->nz = t->n/mpi.nz;
  if (t->n%mpi.nz != 0)
  {
      print_error("Wrong number of bins in z dimension");
  }
#endif
  t->dx = parameters->gx/t->n;
  t->dy = parameters->gy/t->n;
  t->dz = parameters->gz/t->n;
  
#ifdef USEMPI
  t->flux = calloc(t->nx*t->ny*t->nz, sizeof(double));
#else
  t->flux = calloc(t->n*t->n*t->n, sizeof(double));
#endif

  return t;
}

Material *init_material(Parameters *parameters)
{
  int i;
  Nuclide sum = {0, 0, 0, 0, 0};

  // Hardwire the material macroscopic cross sections for now to produce a keff
  // close to 1 (fission, absorption, scattering, total, atomic density)
  Nuclide macro = {parameters->xs_f, parameters->xs_a, parameters->xs_s,
     parameters->xs_f + parameters->xs_a + parameters->xs_s, 1.0};

  Material *m = malloc(sizeof(Material));
  m->n_nuclides = parameters->n_nuclides;
  m->nuclides = malloc(m->n_nuclides*sizeof(Nuclide));

  // Generate some arbitrary microscopic cross section values and atomic
  // densities for each nuclide in the material such that the total macroscopic
  // cross sections evaluate to what is hardwired above
  for(i=0; i<m->n_nuclides; i++){
    if(i<m->n_nuclides-1){
      m->nuclides[i].atom_density = rn()*macro.atom_density;
      macro.atom_density -= m->nuclides[i].atom_density;
    }
    else{
      m->nuclides[i].atom_density = macro.atom_density;
    }
    m->nuclides[i].xs_a = rn();
    sum.xs_a += m->nuclides[i].xs_a * m->nuclides[i].atom_density;
    m->nuclides[i].xs_f = rn();
    sum.xs_f += m->nuclides[i].xs_f * m->nuclides[i].atom_density;
    m->nuclides[i].xs_s = rn();
    sum.xs_s += m->nuclides[i].xs_s * m->nuclides[i].atom_density;
  }
  for(i=0; i<m->n_nuclides; i++){
    m->nuclides[i].xs_a /= sum.xs_a/macro.xs_a;
    m->nuclides[i].xs_f /= sum.xs_f/macro.xs_f;
    m->nuclides[i].xs_s /= sum.xs_s/macro.xs_s;
    m->nuclides[i].xs_t = m->nuclides[i].xs_a + m->nuclides[i].xs_s;
  }

  m->xs_f = parameters->xs_f;
  m->xs_a = parameters->xs_a;
  m->xs_s = parameters->xs_s;
  m->xs_t = parameters->xs_a + parameters->xs_s;

  return m;
}

#ifdef USEMPI
void sample_source_particle_pos(Geometry *geometry, Particle *p,
				double x, double y, double z, int i_p)
{
    set_stream(STREAM_OTHER); // this is done to keep the coordinates the same for all processes
    p->type = T_LOCAL;
    p->alive = TRUE;
    p->mu = rn()*2 - 1;
    p->phi = rn()*2*PI;
    p->u = p->mu;
    p->v = sqrt(1 - p->mu*p->mu)*cos(p->phi);
    p->w = sqrt(1 - p->mu*p->mu)*sin(p->phi);
    p->x = x;
    p->y = y;
    p->z = z;
    set_stream(STREAM_INIT); // this is done to keep the coordinates the same for all processes
    return;
}

Bank *init_source_bank(Parameters *parameters, Geometry *geometry)
{
  unsigned long i_p; // index over particles
  Bank *source_bank;

  // Initialize source bank, assume that particles are initially
  // distributed uniformly
  source_bank = init_bank(parameters->n_particles/mpi.size);

  // Sample source particles
  for(i_p=0; i_p<parameters->n_particles; i_p++){
      double x = rn()*geometry->x;
      double y = rn()*geometry->y;
      double z = rn()*geometry->z;
      
      if (x >= geometry->x_min && x < geometry->x_max &&
	  y >= geometry->y_min && y < geometry->y_max &&
	  z >= geometry->z_min && z < geometry->z_max)
      {
	  while (source_bank->n >= source_bank->sz)
	  {
	      source_bank->resize(source_bank);
	  }
	  sample_source_particle_pos(geometry, &(source_bank->p[source_bank->n]), x, y, z, i_p);
	  source_bank->n++;
      }
      
  }

  return source_bank;
}

Bank *init_fission_bank(Parameters *parameters)
{
  Bank *fission_bank;
  fission_bank = init_bank(2*parameters->n_particles/mpi.size);

  return fission_bank;
}
#else
Bank *init_source_bank(Parameters *parameters, Geometry *geometry)
{
  unsigned long i_p; // index over particles
  Bank *source_bank;

  // Initialize source bank
  source_bank = init_bank(parameters->n_particles);

  // Sample source particles
  for(i_p=0; i_p<parameters->n_particles; i_p++){
    sample_source_particle(geometry, &(source_bank->p[i_p]));
    source_bank->n++;
  }

  return source_bank;
}

Bank *init_fission_bank(Parameters *parameters)
{
  Bank *fission_bank;
  fission_bank = init_bank(2*parameters->n_particles);

  return fission_bank;
}
#endif

Bank *init_bank(unsigned long n_particles)
{
  Bank *b = malloc(sizeof(Bank));
  b->p = malloc(n_particles*sizeof(Particle));
  b->sz = n_particles;
  b->n = 0;
  b->resize = resize_particles;

  return b;
}

void sample_source_particle(Geometry *geometry, Particle *p)
{
  p->alive = TRUE;
  p->mu = rn()*2 - 1;
  p->phi = rn()*2*PI;
  p->u = p->mu;
  p->v = sqrt(1 - p->mu*p->mu)*cos(p->phi);
  p->w = sqrt(1 - p->mu*p->mu)*sin(p->phi);
  p->x = rn()*geometry->x;
  p->y = rn()*geometry->y;
  p->z = rn()*geometry->z;

  return;
}

void sample_fission_particle(Particle *p, Particle *p_old)
{
#ifdef USEMPI
  p->type = T_LOCAL;
#endif
  p->alive = TRUE;
  p->mu = rn()*2 - 1;
  p->phi = rn()*2*PI;
  p->u = p->mu;
  p->v = sqrt(1 - p->mu*p->mu)*cos(p->phi);
  p->w = sqrt(1 - p->mu*p->mu)*sin(p->phi);
  p->x = p_old->x;
  p->y = p_old->y;
  p->z = p_old->z;

  return;
}

void resize_particles(Bank *b)
{
  b->p = realloc(b->p, sizeof(Particle)*1.1*b->sz);
  b->sz = 1.1*b->sz;

  return;
}

void free_bank(Bank *b)
{
  free(b->p);
  b->p = NULL;
  free(b);
  b = NULL;

  return;
}

void free_material(Material *material)
{
  free(material->nuclides);
  material->nuclides = NULL;
  free(material);
  material = NULL;

  return;
}

void free_tally(Tally *tally)
{
  free(tally->flux);
  tally->flux = NULL;
  free(tally);
  tally = NULL;

  return;
}
