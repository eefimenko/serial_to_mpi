#ifndef HEADER
#define HEADER

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<sys/time.h>
#include<math.h>
#include<float.h>
#include<unistd.h>
#include<string.h>

#ifdef USEMPI
#include <mpi.h>
#define T_LOCAL 0  // type of action: nothing to do
#define T_COLLISION 1 // type of action: collision occurs
#define T_BOUNDARY 2 // type of action: particle crosses boundary
#define SR_BANK_SIZE 2048 // initial size of sen_bank
#endif

#define TRUE 1
#define FALSE 0

// Constants
#define PI 3.1415926535898
#define D_INF DBL_MAX

// Geometry boundary conditions
#define VACUUM 0
#define REFLECT 1
#define PERIODIC 2

// Reaction types
#define TOTAL 0
#define ABSORPTION 1
#define SCATTER 2
#define FISSION 3

// Surfaces
#define X0 0
#define X1 1
#define Y0 2
#define Y1 3
#define Z0 4
#define Z1 5

// RNG streams
#define N_STREAMS 3
#define STREAM_INIT 0
#define STREAM_TRACK 1
#define STREAM_OTHER 2

typedef struct Parameters_{
  unsigned long long seed; // RNG seed
  unsigned long n_particles; // number of particles
  int n_batches; // number of batches
  int n_generations; // number of generations per batch
  int n_active; // number of active batches
  int bc; // boundary conditions
  int n_nuclides; // number of nuclides in material
  int tally; // whether to tally
  int n_bins; // number of bins in each dimension of mesh
  double nu; // average number of fission neutrons produced
  double xs_a; // absorption macro xs
  double xs_s; // scattering macro xs
  double xs_f; // fission macro xs
  double gx; // geometry size in x
  double gy; // geometry size in y
  double gz; // geometry size in z
  int write_tally; // whether to output tallies
  int write_keff; // whether to output keff
  char *tally_file; // path to write tallies to
  char *keff_file; // path to write keff to
#ifdef USEMPI
    int mpi_nx; // number of domains in x dimension
    int mpi_ny; // number of domains in x dimension
    int mpi_nz; // number of domains in x dimension
#endif
} Parameters;

typedef struct Particle_{
    double mu; // cosine of polar angle
    double phi; // azimuthal angle
    double u; // direction
    double v;
    double w;
    double x; // position
    double y;
    double z;
#ifdef USEMPI
    int type; // type of particle: stores the action that should be made on particle after exchange
#endif
    int alive;
    int surface_crossed;
  
} Particle;

typedef struct Geometry_{
  int bc;
  double x;
  double y;
  double z;
#ifdef USEMPI
    double dx; // size of local domain in x dimension
    double dy; // size of local domain in y dimension
    double dz; // size of local domain in z dimension
    double x_min; // minimal x for local domain
    double y_min; // minimal y for local domain
    double z_min; // minimal z for local domain
    double x_max; // maximal x for local domain
    double y_max; // maximal y for local domain
    double z_max; // maximal z for local domain
#endif
} Geometry;

typedef struct Nuclide_{
  double xs_f; // fission micro xs
  double xs_a; // absorption micro xs
  double xs_s; // scattering micro xs
  double xs_t; // total micro xs
  double atom_density; // atomic density of nuclide in material
} Nuclide;

typedef struct Material_{
  double xs_f; // fission macro xs
  double xs_a; // absorption macro xs
  double xs_s; // scattering macro xs
  double xs_t; // total macro xs
  int n_nuclides;
  Nuclide *nuclides;
} Material;

typedef struct Tally_{
  int tallies_on; // whether tallying is currently turned on
  int n; // mumber of grid boxes in each dimension
#ifdef USEMPI
    int nx; // mumber of local grid boxes in x dimension
    int ny; // mumber of local grid boxes in x dimension
    int nz; // mumber of local grid boxes in x dimension
#endif
  double dx; // grid spacing
  double dy;
  double dz;
  double *flux;
} Tally;

typedef struct Bank_{
  unsigned long n; // number of particles
  unsigned long sz; // size of bank
  Particle *p; // particle array
  void (*resize)(struct Bank_ *b);
} Bank;

// io.c function prototypes
void parse_parameters(Parameters *parameters);
void read_CLI(int argc, char *argv[], Parameters *parameters);
void print_error(char *message);
void print_parameters(Parameters *parameters);
void border_print(void);
void fancy_int(long a);
void center_print(const char *s, int width);
void print_status(int i_a, int i_b, double keff_batch, double keff_mean, double keff_std);
void init_output(Parameters *parameters);
void write_tally(Tally *t, char *filename);
void write_keff(double *keff, int n, char *filename);

// utils.c funtion prototypes
double timer(void);

// prng.c function prototypes
double rn(void);
int rni(int min, int max);
void set_stream(int rn_stream);
void set_initial_seed(unsigned long long rn_seed0);
void rn_skip(long long n);

// initialize.c function prototypes
Parameters *init_parameters(void);
Geometry *init_geometry(Parameters *parameters);
Tally *init_tally(Parameters *parameters);
Material *init_material(Parameters *parameters);
Bank *init_fission_bank(Parameters *parameters);
Bank *init_source_bank(Parameters *parameters, Geometry *geometry);
Bank *init_bank(unsigned long n_particles);
void sample_source_particle(Geometry *geometry, Particle *p);
void sample_fission_particle(Particle *p, Particle *p_old);
void resize_particles(Bank *b);
void free_bank(Bank *b);
void free_material(Material *material);
void free_tally(Tally *tally);

// transport.c function prototypes
void transport(Parameters *parameters, Geometry *geometry, Material *material, Bank *source_bank, Bank *fission_bank, Tally *tally, Particle *p);
double distance_to_boundary(Geometry *geometry, Particle *p);
double distance_to_collision(Material *material);
void cross_surface(Geometry *geometry, Particle *p);
void collision(Material *material, Bank *fission_bank, double nu, Particle *p);

// eigenvalue.c function prototypes
void run_eigenvalue(Parameters *parameters, Geometry *geometry, Material *material, Bank *source_bank, Bank *fission_bank, Tally *tally, double *keff);
void synchronize_bank(Bank *source_bank, Bank *fission_bank);
void calculate_keff(double *keff, double *mean, double *std, int n);

// tally.c function prototypes
void score_tally(Parameters *parameters, Material *material, Tally *tally, Particle *p);
void reset_tally(Tally *tally);

#ifdef USEMPI

typedef struct MPI_data_
{
    int size; // size of communicator, the full number of processes (set in configuration)
    int rank; // current process rank
    int nx; // the full number of domains in x dimension (set in configuration)
    int ny; // the full number of domains in y dimension (set in configuration)
    int nz; // the full number of domains in z dimension (set in configuration)
    int i; // domain index along x dimension: [0,nx) - calculated based on rank and size
    int j; // domain index along y dimension: [0,ny) - calculated based on rank and size
    int k; // domain index along z dimension: [0,nz) - calculated based on rank and size
    Bank** send_bank; // auxilary array of Banks with size number of elements, in send_bank[i]
                      // the particles the moves to i-th domain are stored, note that
                      // send_bank[rank] is always empty as particle that stays in the same domain
                      // are not sent anywhere
    Bank* send_buf;   // actual send buffer that is used in particles exchange, data is copied from send_bank[i] arrays
    Bank* recv_buf;   // actual receive buffer that is used in particles exchange, from this buffer particles are copied into source_bank
    int* send_num; // auxilary array, send_num[i] stores number of particles to be sent to i-th domain, note that send_num[rank]
                   // always equal 0. Used in update_number_of_exchanged_particles()
    int* recv_num; // auxilary array, recv_num[i] stores number of particles to be received from i-th domain, note that recv_num[rank]
                   // always equal 0. Used in update_number_of_exchanged_particles()
    int* send_off; //auxilary array, stores offset for i-th domain in send buffer send_buf, calculated based on send_num
    int* recv_off; //auxilary array, stores offset for i-th domain in send buffer recv_buf, calculated based on recv_num
    MPI_Datatype MPI_prtcl; // MPI datatype representing particle type, used in exchange_particles()
} MPI_data;

void init_mpi(int argc, char *argv[], MPI_data* mpi_p); // initialises all mpi related buffers and types
void finalize_mpi(); // frees all mpi related buffers 
int check_rank(double x, double y, double z, Geometry* g); // check to which domain the point with coordinates x,y,z belongs
void update_number_of_exchanged_particles(); // Updates number of particles to be exchanged in all processes
void exchange_particles(Parameters *parameters, Geometry *geometry, Material *material, Bank *source_bank, Bank *fission_bank, Tally *tally);
int check_number_of_alive_particles(Bank *source_bank); // Calculates the number of alive particles
int check_alive_particles(Bank *source_bank); // Checks if there are any alive particles in simulation area

extern MPI_data mpi;
#endif

#endif
