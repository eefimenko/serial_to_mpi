#include "header.h"
// This function is used to
// 1. Initialise the mpi structure
// 2. Allocate all banks and buffers
// 3. Create MPI_datatype used later for particles exchange
void init_mpi(int argc, char *argv[], MPI_data* mpi_p)
{
    int i;
// 1. Initialise the mpi structure
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_p->size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_p->rank);
// 2. Allocate all banks and buffers
    mpi.send_bank = malloc(mpi_p->size*sizeof(Bank*));
    mpi.send_buf = init_bank(SR_BANK_SIZE);
    mpi.recv_buf = init_bank(SR_BANK_SIZE);
    mpi.send_num = malloc(mpi_p->size*sizeof(int));
    mpi.recv_num = malloc(mpi_p->size*sizeof(int));
    mpi.send_off = malloc(mpi_p->size*sizeof(int));
    mpi.recv_off = malloc(mpi_p->size*sizeof(int));
    memset(mpi.send_num, 0, mpi_p->size*sizeof(int));
    memset(mpi.recv_num, 0, mpi_p->size*sizeof(int));
    memset(mpi.send_off, 0, mpi_p->size*sizeof(int));
    memset(mpi.recv_off, 0, mpi_p->size*sizeof(int));
    
    for (i=0; i< mpi.size; i++)
    {
	if (i != mpi.rank)
	{
	    mpi.send_bank[i] = init_bank(SR_BANK_SIZE);
	}
	else
	{
	    mpi.send_bank[i] = init_bank(0);
	}
    }
    
// 3. Create MPI_datatype used later for particles exchange
    MPI_Datatype types[2];
    MPI_Aint    offsets[2], extent;
    int          blockcounts[2];
    /* Setup description of the 8 MPI_DOUBLE fields */
    offsets[0] = 0;
    types[0] = MPI_DOUBLE;
    blockcounts[0] = 8;

    /* Setup description of the 3 MPI_INT fields n, type */
    /* Need to first figure offset by getting size of MPI_DOUBLE */
    MPI_Type_extent(MPI_DOUBLE, &extent);
    offsets[1] = 8 * extent;
    types[1] = MPI_INT;
    blockcounts[1] = 3;

    /* Now define structured type and commit it */
    MPI_Type_struct(2, blockcounts, offsets, types, &mpi.MPI_prtcl);
    MPI_Type_commit(&mpi.MPI_prtcl);

}
// frees all mpi related buffers 
void finalize_mpi()
{
    int i;
    for (i=0; i< mpi.size; i++)
    {
	    free_bank(mpi.send_bank[i]);
    }
    free_bank(mpi.send_buf);
    free_bank(mpi.recv_buf);
    free(mpi.send_bank);
    free(mpi.send_num);
    free(mpi.recv_num);
    free(mpi.send_off);
    free(mpi.recv_off);
    MPI_Finalize();
}
// check to which domain the point with coordinates x,y,z belongs
// used to determine if particle should be moved to another domain
int check_rank(double x, double y, double z, Geometry* g)
{
    int i = 0,j = 0,k = 0;
    
    i = (int)floor(fabs(x)/g->dx);
    j = (int)floor(fabs(y)/g->dy);
    k = (int)floor(fabs(z)/g->dz);
    if (x < 0)
    {
	i = 0;
    }
    if (x >= g->x)
    {
	i = mpi.nx - 1;
    }
    if (y < 0)
    {
	j = 0;
    }
    if (y >= g->y)
    {
	j = mpi.ny - 1;
    }
    if (z < 0)
    {
	k = 0;
    }
    if (z >= g->z)
    {
	k = mpi.nz - 1;
    }
    int ans = k*mpi.nx*mpi.ny + j*mpi.nx + i;
    
    if (ans < 0 || ans >= mpi.size)
    {
	printf("Wrong rank %d for coordinates %lf %lf %lf\n", ans, x, y, z);
	exit(-1);
    }
    return ans;
}

// Updates number of particles to be exchanged in all processes
void update_number_of_exchanged_particles()
{
    int i;
    mpi.send_off[0] = 0;
    mpi.recv_off[0] = 0;
    // First update the number of particles to be sent to each domain,
    // then reset the send_bank and update offset according to number of particles
    for (i=0; i< mpi.size; i++)
    {
	mpi.send_num[i] = mpi.send_bank[i]->n;
	mpi.send_bank[i]->n = 0;
	
	if (i > 0)
	{
	    mpi.send_off[i] = mpi.send_off[i-1] + mpi.send_num[i-1];
	}
    }
    // Perform exchange all-to-all to gather th number of particles to be
    // received from each process
    MPI_Alltoall(mpi.send_num, 1, MPI_INT, mpi.recv_num, 1,
		 MPI_INT, MPI_COMM_WORLD);
    // update recv offset according to received number of particles	  
    for (i=0; i< mpi.size; i++)
    {
	if (i > 0)
	{
	    mpi.recv_off[i] = mpi.recv_off[i-1] + mpi.recv_num[i-1];
	}
    }
}

// Performs actual particles exchange in all processes
void exchange_particles(Parameters *parameters, Geometry *geometry, Material *material, Bank *source_bank, Bank *fission_bank, Tally *tally)
{
    int i;
    // Calculate total number of particles to be sent to all processes
    int total_send = mpi.send_off[mpi.size-1] + mpi.send_num[mpi.size-1];
    // Calculate total number of particles to be received from all processes
    int total_recv = mpi.recv_off[mpi.size-1] + mpi.recv_num[mpi.size-1];

    // First, adjust the sizes of recv_buf and send_buf, so all particles fit in memory
    while (total_send >= mpi.send_buf->sz)
    {
	mpi.send_buf->resize(mpi.send_buf);
    }
    
    while (total_recv >= mpi.recv_buf->sz)
    {
	mpi.recv_buf->resize(mpi.recv_buf);
    }

    // Prepare send_buf by copying particles data from auxilary send_bank to contigious array
    for (i=0; i< mpi.size; i++)
    {
	memcpy(mpi.send_buf->p + mpi.send_off[i], mpi.send_bank[i]->p, mpi.send_num[i]*sizeof(Particle));
    }
    // Perform actual exchange of particles all-to-all
    MPI_Alltoallv(mpi.send_buf->p, mpi.send_num, mpi.send_off, mpi.MPI_prtcl,
		  mpi.recv_buf->p, mpi.recv_num, mpi.recv_off, mpi.MPI_prtcl, MPI_COMM_WORLD);
    
    // Adjust the size of source bank, and copy received particles there
    while(source_bank->n + total_recv >= source_bank->sz)
    {
	source_bank->resize(source_bank);
    }
    
    int n0 = source_bank->n;
    memcpy(&(source_bank->p[source_bank->n]), mpi.recv_buf->p, total_recv*sizeof(Particle));
    source_bank->n += total_recv;
    // Received particles are processed one-by-one according to stored action
    for (i = 0; i < total_recv; i++)
    {
	Particle* p = &(source_bank->p[n0 + i]);

	if (p->type == T_BOUNDARY)
	{
	    cross_surface(geometry, p);
	}
	else
	{
	    collision(material, fission_bank, parameters->nu, p);
	}
	p->type = T_LOCAL;
    }
}

// Calculates the number of alive particles
int check_number_of_alive_particles(Bank *source_bank)
{
    int i;
    int num = 0;
    int num_g;
    
    int total_send = mpi.send_off[mpi.size-1] + mpi.send_num[mpi.size-1];
    int total_recv = mpi.recv_off[mpi.size-1] + mpi.recv_num[mpi.size-1];
    int ts,tr;
    
    for (i=0; i<source_bank->n; i++)
    {
	if (source_bank->p[i].alive == TRUE)
	{
	    num++;
	}
    }
    
    MPI_Allreduce(&num, &num_g, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    MPI_Reduce(&total_send, &ts, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&total_recv, &tr, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    
    if (mpi.rank == 0)
    {
	printf("Number of alive particles %d. On previous step: total send %d, total receive %d\n", num_g, ts, tr);
    }
    return num_g;
}

// Checks if there are any alive particles in simulation area
int check_alive_particles(Bank *source_bank)
{
    int i;
    int num = 0;
    int num_g;
    
    for (i=0; i<source_bank->n; i++)
    {
	if (source_bank->p[i].alive == TRUE)
	{
	    num++;
	    break;
	}
    }
    
    MPI_Allreduce(&num, &num_g, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

    return num_g;
}


