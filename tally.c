#include "header.h"

// Simple flux tally
void score_tally(Parameters *parameters, Material *material, Tally *tally, Particle *p)
{
  int ix, iy, iz;
  double vol;

  // Volume
  vol = tally->dx * tally->dy * tally->dz;
  
#ifdef USEMPI
// Find the indices of the grid box of the particle
  double xmin = parameters->gx/mpi.nx*mpi.i;
  double ymin = parameters->gy/mpi.ny*mpi.j;
  double zmin = parameters->gz/mpi.nz*mpi.k; 
  ix = (p->x - xmin)/tally->dx; // calculate local tally x coordinate
  iy = (p->y - ymin)/tally->dy; // calculate local tally y coordinate
  iz = (p->z - zmin)/tally->dz; // calculate local tally z coordinate

  // Scalar flux
  tally->flux[ix + tally->nx*iy + tally->nx*tally->ny*iz] += 1./(vol * material->xs_t * parameters->n_particles);
#else
  // Find the indices of the grid box of the particle
  ix = p->x/tally->dx;
  iy = p->y/tally->dy;
  iz = p->z/tally->dz;

  // Scalar flux
  tally->flux[ix + tally->n*iy + tally->n*tally->n*iz] += 1./(vol * material->xs_t * parameters->n_particles);
#endif
  
  return;
}

void reset_tally(Tally *tally)
{
#ifdef USEMPI
    memset(tally->flux, 0, tally->nx*tally->ny*tally->nz*sizeof(double));
#else
    memset(tally->flux, 0, tally->n*tally->n*tally->n*sizeof(double));
#endif
  return;
}
